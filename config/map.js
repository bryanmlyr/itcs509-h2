module.exports = {
  arad: {
    name: 'Arad',
    pos: { x: 0, y: 0 },
    linked_to: ['Fagaras', 'Sibiu', 'Zerind', 'Vaslul']
  },
  zerind: {
    name: 'Zerind',
    pos: { x: 10, y: 0 },
    linked_to: ['Arad', 'Sibiu', 'Issi']
  },
  sibiu: {
    name: 'Sibiu',
    pos: { x: 5, y: 5 },
    linked_to: ['Arad', 'Zerind', 'Fagaras', 'Vaslul', 'Issi']
  },
  fagaras: {
    name: 'Fagaras',
    pos: { x: 2, y: 7 },
    linked_to: ['Arad', 'Cravos', 'Vaslul']
  },
  cravos: {
    name: 'Cravos',
    pos: { x: 0, y: 10 },
    linked_to: ['Vaslul', 'Fagaras']
  },
  vaslul: {
    name: 'Vaslul',
    pos: { x: 5, y: 8 },
    linked_to: ['Issi', 'Fagaras', 'Sibiu']
  },
  issi: {
    name: 'Issi',
    pos: { x: 10, y: 10 },
    linked_to: ['Vaslul', 'Sibiu', 'Zerind']
  }
};
