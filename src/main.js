import map from '../config/map';
import { bfs, dfs, greedy, astar } from './algos';

console.log('bfs');
console.log('-----------------');
bfs(map.arad, map.issi, map);
console.log('-----------------\n');

console.log('dfs');
console.log('-----------------');
dfs(map.arad, map.issi, map);
console.log('-----------------\n');

console.log('greedy');
console.log('-----------------');
greedy(map.arad, map.issi, map);
console.log('-----------------\n');

console.log('astar');
console.log('-----------------');
astar(map.arad, map.issi, map);
console.log('-----------------\n');
