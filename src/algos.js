function bfs(start, end, map) {
  let queue = [];
  let visited = [];

  queue.push(start);
  visited.push(start.name);

  console.log(start.name);
  while (queue.length != 0) {
    let node = queue.shift();

    for (let x = 0; x < node.linked_to.length; x++) {
      console.log(node.linked_to[x]);

      if (visited.indexOf(node.linked_to[x]) == -1) {
        visited.push(node.linked_to[x]);
        queue.push(map[node.linked_to[x].toLowerCase()]);
      }

      if (node.linked_to[x] == end.name) return;
    }
  }
}

function dfsRec(currNode, end, map, visited) {
  for (let x = 0; x < currNode.linked_to.length; x++) {
    if (currNode.name == end.name) return;
    if (visited.indexOf(currNode.linked_to[x]) == -1) {
      console.log(currNode.linked_to[x]);

      visited.push(currNode.linked_to[x]);
      return dfsRec(map[currNode.linked_to[x].toLowerCase()], end, map, visited);
    }
  }
}

function dfs(start, end, map) {
  let visited = [];

  console.log(start.name);
  visited.push(start.name);

  dfsRec(start, end, map, visited);
}

function greedy(start, end, map) {
  let queue = [];
  let finalPos = {
    posX: end.pos.x,
    posY: end.pos.y
  };

  queue.push(start);
  console.log(start.name);

  while (queue.length != 0) {
    let node = queue.shift();

    // Distance between next city and the arrival
    let posHeuri = [];

    for (let x = 0; x < node.linked_to.length; x++) {
      posHeuri.push({
        name: node.linked_to[x],
        heuri: Math.abs(finalPos.posX - map[node.linked_to[x].toLowerCase()].pos.x + (finalPos.posY - map[node.linked_to[x].toLowerCase()].pos.y))
      });

      if (node.linked_to[x] == end.name) {
        console.log(node.linked_to[x]);
        return;
      }
    }
    posHeuri = posHeuri.sort((x, y) => {
      return x.heuri - y.heuri;
    });
    console.log(posHeuri[0].name);
    queue.push(map[posHeuri[0].name.toLowerCase()]);
  }
}

function astar(start, end, map) {
  let queue = [];
  let finalPos = {
    posX: end.pos.x,
    posY: end.pos.y
  };

  queue.push(start);
  console.log(start.name);

  while (queue.length != 0) {
    let node = queue.shift();

    // Distance between next city and the arrival
    let posHeuriCost = [];

    for (let x = 0; x < node.linked_to.length; x++) {
      posHeuriCost.push({
        name: node.linked_to[x],
        cost: Math.abs(node.pos.x - map[node.linked_to[x].toLowerCase()].pos.x + (node.pos.y - map[node.linked_to[x].toLowerCase()].pos.y)),
        heuri: Math.abs(finalPos.posX - map[node.linked_to[x].toLowerCase()].pos.x + (finalPos.posY - map[node.linked_to[x].toLowerCase()].pos.y))
      });

      if (node.linked_to[x] == end.name) {
        console.log(node.linked_to[x]);
        return;
      }
    }
    posHeuriCost = posHeuriCost.sort((x, y) => {
      return x.cost - x.heuri + y.cost - y.heuri;
    });
    console.log(posHeuriCost[0].name);
    queue.push(map[posHeuriCost[0].name.toLowerCase()]);
  }
}

module.exports = {
  dfs,
  bfs,
  greedy,
  astar
};
